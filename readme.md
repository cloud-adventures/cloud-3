# Cloud Computing 3
<figure align = "center">
<img src="images/banner3.png" alt="Trulli" width="800" height="423"">
</figure>

## Name
News Sentiment Analaysis with Lambda and Terraform. 

## Description
Project takes ideas from online courses, websites and articles to produce a working simple application.  It:
- Creates AWS Architecture in Terraform
- Package Lambda dependencies with Terraform  
- Utilise AWS Machine Learning.
- Deploy Lambda Functions

Lambda function is triggered every minute to query news api.  This info is passed to Comprehend for sentiment analysis and the info is stored to DynamoDB table. 

## AWS Architecture

<figure align = "center">
<img src="images/cloud3.png" alt="AWS " width="800" height="397"">
<figcaption align = "center"><b>AWS Architecture</b></figcaption>
</figure>

## Key Files
- `lambda.py` - triggered every minute by cloudwatch event rule to query API for news stories and write or delete to DynamoDB table.  File is packaged by executing script via Terraform 
- `lambda.tf` - contains lambda based resources
- `iam.tf` - lambda permissions and role
- `create_pkg.sh` - create deployment package for dependencies



 When the architecture has been built navigate to DNS name or Load Balancer ARN to make predicitons with the app.   

## Results
<figure align = "center">
<img src="images/items.png" alt="AWS " width="800" height="596"">
<figcaption align = "center"><b>DynamoDB items</b></figcaption>
</figure>

<br/><br/>


## Contributing
- [Terraform Lambda dependencies](https://alek-cora-glez.medium.com/deploying-aws-lambda-function-with-terraform-custom-dependencies-4407cd4fc)
- [AWS Serverless](https://www.udemy.com/course/aws-serverless-a-complete-guide/)

## Project status
Project is ongoing:
- utilise API gateway to query endpoint from table



