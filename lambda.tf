resource "null_resource" "install_python_dependencies" {
  provisioner "local-exec" {
    command = "bash ${path.module}/scripts/create_pkg.sh"

    environment = {
      source_code_path = var.path_source_code
      function_name    = var.function_name
      path_module      = path.module
      runtime          = var.runtime
      path_cwd         = path.cwd
    }
  }
}

data "archive_file" "create_dist_pkg" {
  depends_on  = [null_resource.install_python_dependencies]
  source_dir  = "${path.cwd}/lambda_dist_pkg/"
  output_path = var.output_path
  type        = "zip"
}

resource "aws_lambda_function" "aws_lambda_test" {
  function_name = var.function_name
  description   = "process sentiment"
  handler       = "lambda_function.lambda.lambda_handler"
  runtime       = var.runtime

  role        = aws_iam_role.lambda_exec_role.arn
  memory_size = 128
  timeout     = 300

  depends_on       = [null_resource.install_python_dependencies]
  source_code_hash = data.archive_file.create_dist_pkg.output_base64sha256
  filename         = data.archive_file.create_dist_pkg.output_path
}
resource "aws_cloudwatch_event_rule" "lambda_invoke" {
  name                = "invoke-lambda"
  description         = "start lambda"
  schedule_expression = "rate(1 minute)"
}
resource "aws_cloudwatch_event_target" "lambda_invoke" {
  rule      = aws_cloudwatch_event_rule.lambda_invoke.name
  target_id = "lambda_invoke"
  arn       = aws_lambda_function.aws_lambda_test.arn
}
resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.aws_lambda_test.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.lambda_invoke.arn
}
