# VPC ID
output "source_code_path" {
  description = "source code path"
  value       = var.path_source_code
}

# VPC CIDR blocks
output "function_name" {
  description = "function_name"
  value       = var.function_name
}

# VPC ID
output "path_module" {
  description = "path_module"
  value       = path.module
}

# VPC CIDR blocks
output "runtime" {
  description = "runtime"
  value       = var.runtime
}

# VPC CIDR blocks
output "path_cwd" {
  description = "path_cwd"
  value       = path.cwd
}

